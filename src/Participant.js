import React from 'react';
import PropTypes from 'prop-types';
import playerDefaultAvatar from './img/player__avatar_default.jpg';


function Participant(props) {
	const {image = playerDefaultAvatar, name, hp, maxHp, shield, type, horror = 0} = props;
	
	return (
		<div className='row align-items-center shadow bg-light rounded no-gutters'>
			<div className='col-3'>
				<img src={image} className='participant__avatar img-fluid rounded-left' alt={type}/>
			</div>
			<div className='col ml-4'>
				<header className='h3'>{name}</header>
				<ul className='list-inline'>
					<li className='list-inline-item'>HP: {`${hp}/${maxHp}`}</li>
					<li className='list-inline-item'>Shield: {shield}</li>
					{ type === 'Player' ? <li className='list-inline-item'>Horror: {horror}</li> : null}
				</ul>
			</div>
		</div>
	);
}

Participant.propTypes = {
	image:PropTypes.string,
	name:PropTypes.string.isRequired,
	hp:PropTypes.number.isRequired,
	maxHp:PropTypes.number.isRequired,
	shield: PropTypes.number.isRequired,
	type: PropTypes.string.isRequired,
	horror: PropTypes.number
}

export default Participant