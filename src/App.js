import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { createGame } from './utils/api'
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'
import Login from './Login'
import Gameboard from './Gameboard'


class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			gameId: '',
			playerName: '',
			currentTurn:0,
			maxTurns:20,
			turnsLeft:20
		};
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}

	handleSubmit(e) {
		const {playerName} = this.state;
		createGame(playerName).then(({ id:gameId, currentTurn, maxTurns, turnsLeft }) => {
			this.setState({gameId, currentTurn, maxTurns, turnsLeft});
		});
		e.preventDefault();
	}

	handleChange(e) {
		const {value} = e.target;
		this.setState({playerName: value});
	}

	render() {
		const {gameId, playerName, currentTurn, maxTurns, turnsLeft} = this.state;

		return (
			<div className='p-4 p-md-5'>
				<Router>
					<Switch>
						<Route exact path='/' render={() => (
							<Login handleChange={this.handleChange} handleSubmit={this.handleSubmit} playerName={playerName} gameId={gameId}/>
						)}/>
						<Route exact path='/Gameboard/:gameId' component={({match}) => (
							<Gameboard match={match} playerName={playerName} currentTurn={currentTurn} maxTurns={maxTurns} turnsLeft={turnsLeft}/>
						)}/>
						<Route render={() => <p>Not Found</p>}/>
					</Switch>
				</Router>
			</div>
		);
	}
}

export default App;