import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getGameMonster, getGamePlayer, getGame, getPlayerCards, nextTurn } from './utils/api';
import TurnTracker from './TurnTracker';
import Participant from './Participant';
import PlayerCards from './PlayerCards';
import Modal from './Modal';
import './Gameboard.css';


function handleHorror({effect, value}) {
	return effect === 'HORROR' ? value : 0;
}

class Gameboard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			gameId: this.props.match.params.gameId,
			playerName: this.props.playerName,
			currentTurn:this.props.currentTurn,
			maxTurns:this.props.maxTurns,
			turnsLeft:this.props.turnsLeft,
			horror: 0,
			playerCards: [],
			chosenCardId: '',
			player: {
				id:'', 
				name: '', 
				hp: 20, 
				maxHp: 20, 
				shield: 0,
				horror: 0
			},
			monster: {
				id: '', 
				name: '', 
				hp: 40, 
				maxHp: 40, 
				shield: 0, 
				image: ''
			},
			monsterEffect: {effect: '', value: 0}
		};

		this.handleTurn = this.handleTurn.bind(this);
		this.handleCards = this.handleCards.bind(this);
	}

	componentDidMount() {
		const { gameId } = this.state;

		if(gameId) {
			Promise.all([
				getGame(gameId),
				getGameMonster(gameId),
				getGamePlayer(gameId)
			])
			.then(([gameData, monsterData, playerData]) => {
				this.setState({
					gameId: gameData.id, 
					currentTurn: gameData.currentTurn, 
					maxTurns: gameData.maxTurns, 
					turnsLeft: gameData.turnsLeft,
					monster: {id:monsterData, name:monsterData.name, hp:monsterData.hp, maxHp:monsterData.maxHp, shield:monsterData.shield, image:monsterData.image},
					player: {id:playerData.id, name:playerData.name, hp:playerData.hp, maxHp:playerData.maxHp, shield:playerData.shield}
				});
			}).then(() => {
				getPlayerCards(this.state.player.id).then(playerCards => {
					this.setState({playerCards});
				});
			});
		}
	}

	handleTurn() {
		this.setState(function(state) {
			return {
				horror: state.horror ? state.horror - 1 : state.horror
			};
		});

		const { gameId, chosenCardId, player: {id:playerId}  } = this.state;

		Promise.all([
			nextTurn(gameId, chosenCardId),
			getGameMonster(gameId),
			getGamePlayer(gameId),
			getPlayerCards(playerId)
		]).then(([{game, monsterEffect}, monsterData, playerData, playerCards]) => {
			//alert(`Monster plays ${monsterEffect.effect} ${monsterEffect.value}`);

			this.setState((state) => ({
				currentTurn: game.currentTurn,
				maxTurns: game.maxTurns,
				turnsLeft: game.turnsLeft,
				horror: state.horror + handleHorror(monsterEffect),
				monster: {id:monsterData, name:monsterData.name, hp:monsterData.hp, maxHp:monsterData.maxHp, shield:monsterData.shield, image:monsterData.image},
				player: {id:playerData.id, name:playerData.name, hp:playerData.hp, maxHp:playerData.maxHp, shield:playerData.shield},
				playerCards: playerCards,
				monsterEffect: {effect: monsterEffect.effect, value: monsterEffect.value}
			}));
		});
	}

	handleCards(e) {
		const selectedCardId = e.currentTarget.getAttribute('data-id');
		
		this.setState({
			chosenCardId: selectedCardId
		});
	}

	render () {
		const { maxTurns, currentTurn, turnsLeft, playerCards, chosenCardId, monster: {...monster}, horror, player: {...player}, monsterEffect } = this.state;

		if(player.hp < 1) {
			return (
				<Modal>
					<h1 className='h3'>{player.name}, you run out of health and lost to {monster.name}!</h1>
				</Modal>
			);
		} else if (monster.hp < 1) {
			return (
				<Modal>
					<h1 className='h3'>{player.name}, you defeated {monster.name} and win!</h1>
				</Modal>
			);
		} else if (!turnsLeft) {
			return (
				<Modal>
					<h1 className='h3'>{player.name}, you run out of time and lost to {monster.name}!</h1>
				</Modal>
			);
		}

		return (
			<div className='gameboard container'>
				<div className='row no-gutters'>
					<div className='col-9'>
						<div className='container'>
							<div className='row no-gutters'>
								<div className='col-12 mb-4'>
									<Participant type='Monster' image={monster.image} name={monster.name} hp={monster.hp} maxHp={monster.maxHp} shield={monster.shield}/>
								</div>
								<div className='col-12'>
									<Participant type='Player' name={player.name} hp={player.hp} maxHp={player.maxHp} shield={player.shield} horror={horror}/>
								</div>
								<div className='col-12 mt-5'>
									<PlayerCards handleCards={this.handleCards} cards={playerCards} chosenCardId={chosenCardId} horror={horror}/>
								</div>
							</div>
						</div>
					</div>
					<div className='col-3'>
						<TurnTracker handleTurn={this.handleTurn} currentTurn={currentTurn} maxTurns={maxTurns} turnsLeft={turnsLeft} />	
						<div className='bg-light p-3 rounded mt-3'>
							{`Monster plays: ${monsterEffect.effect} ${monsterEffect.value}`}
						</div>
					</div>
				</div>
			</div>

		);
	}
}

Gameboard.propTypes = {
	playerName:PropTypes.string.isRequired,
	currentTurn:PropTypes.number.isRequired,
	maxTurns:PropTypes.number.isRequired,
	turnsLeft:PropTypes.number.isRequired,
	match: PropTypes.object.isRequired
}

export default Gameboard