import React from 'react';
import { Redirect } from 'react-router-dom'
import PropTypes from 'prop-types';
import './Login.css';

function Login(props) {
	const { gameId, playerName, handleSubmit, handleChange } = props;

	if (gameId) {
		return <Redirect to={`/Gameboard/${gameId}`} />
	}

	return (
		<div className='login container shadow rounded bg-light p-5'>
			<header>
				<h1 className='h3'>Welcome to Bons Game</h1>
			</header>
			<main>
				<form onSubmit={handleSubmit} className='form-group'>
					<label htmlFor='playerName'>What's your name?</label>
					<input id='playerName' placeholder='Name' type='text' autoComplete='off' className='form-control mb-3'
						onChange={handleChange} 
						value={playerName}
					/>
					<button type='submit' className='btn btn-primary btn-block'
						disabled={!playerName}>
							Let's Play
					</button>
				</form>
			</main>
		</div>
	)
}

Login.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  playerName: PropTypes.string.isRequired,
  gameId: PropTypes.string.isRequired
}

export default Login