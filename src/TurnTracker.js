import React from 'react';
import PropTypes from 'prop-types';


function TurnTracker(props) {
	const { currentTurn, turnsLeft, handleTurn } = props;
	const pastTurns = currentTurn ? currentTurn - 1 : 0;
	
	return (
		<div className='container rounded bg-light shadow'>
			<div className='p-4 text-center'>
				<header>
					<h1  className='h3'>Turns</h1>
				</header>
				<div className='pt-3 mt-3 mb-3 rounded border'>
					<div>	
						<h2 className='h5'>Current</h2>
						<p>{currentTurn}</p>
					</div>
					<div>	
						<h2 className='h5'>Past</h2>
						<p>{pastTurns}</p>
					</div>
					<div>	
						<h2 className='h5'>Left</h2>
						<p>{turnsLeft}</p>
					</div>
				</div>
				<button className='btn btn-primary' onClick={handleTurn}>End Turn</button>
			</div>
		</div>
	);
}

TurnTracker.propTypes = {
	currentTurn: PropTypes.number.isRequired,
	turnsLeft: PropTypes.number.isRequired,
	handleTurn: PropTypes.func.isRequired
}

export default TurnTracker