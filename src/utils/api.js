import axios from 'axios';


const URL = 'http://game.bons.me/api';

export function createGame (name) { // OK
	return axios.post(`${URL}/games`, {name}).then(({data:gameData}) => gameData);
	/*{
		"id":"ac6ce763-2c38-43db-adf3-290baabc07b5",
		"currentTurn":0,
		"maxTurns":20,
		"turnsLeft":20,
		"createdAt":"2018-09-17T19:32:47.583Z",
		"updatedAt":"2018-09-17T19:32:47.583Z"
	}*/
}

export function getGame (gameId) { // OK
	return axios.get(`${URL}/games/${gameId}`).then(({data:gameData}) => gameData);
	/*{
		"id": "8e975b7a-2355-4f51-830b-68e4211b80f3",
		"currentTurn": 0,
		"maxTurns": 20,
		"turnsLeft": 20,
		"createdAt": "2018-09-19T14:57:46.747Z",
		"updatedAt": "2018-09-19T14:57:46.747Z"
	}*/
}

export function getGameMonster (gameId) { // OK
	return axios.get(`${URL}/games/${gameId}/monster`).then(({data:monsterData}) => monsterData);	
	/*{
		"id": "1253768d-a755-4e43-b767-63f25429848e",
		"hp": 40,
		"maxHp": 40,
		"shield": 0,
		"name": "Cthulhu",
		"image": "https://vignette.wikia.nocookie.net/lovecraft/images/c/cf/Screenshot_20171018-093500.jpg/revision/latest?cb=20171020174137",
		"createdAt": "2018-09-18 19:20:05.281 +00:00",
		"updatedAt": "2018-09-18 19:20:05.283 +00:00",
		"gameId": "9729da5a-e7a4-44dc-8c3d-0105bec21757"
	}*/
}

export function getGamePlayer (gameId) { // OK
	return axios.get(`${URL}/games/${gameId}/player`).then(({data: playerData}) => playerData);	
	/*{
		"id":"48b7a4f5-ddb5-4c3a-b625-8359e9c95347",
		"hp":20,
		"maxHp":20,
		"shield":0,
		"name":"MartinM",
		"createdAt":"2018-09-18T19:20:05.247Z",
		"updatedAt":"2018-09-18T19:20:05.248Z",
		"gameId":"9729da5a-e7a4-44dc-8c3d-0105bec21757"
	}*/
}

export function getPlayerCards (playerId) { // OK
	return axios.get(`${URL}/players/${playerId}/cards`).then(({data:playerCardsData}) => playerCardsData);
	/*[
		{
			"id":"2bd79f0f-489e-4180-a717-31e5eaf7108d",
			"value":3,
			"effect":"DAMAGE",
			"createdAt":"2018-09-19 14:57:46.757 +00:00",
			"updatedAt":"2018-09-19 14:57:46.759 +00:00"
		},
		{
			"id":"200a5cfe-7951-4235-bde9-1a15f6b39c3b","
			value":5,
			"effect":"DAMAGE",
			"createdAt":"2018-09-19 14:57:46.760 +00:00",
			"updatedAt":"2018-09-19 14:57:46.762 +00:00"
		},
		{
			"id":"f2938e04-9332-4d36-8fdb-6ae192b4f152",
			"value":2,
			"effect":"HEAL",
			"createdAt":"2018-09-19 14:57:46.763 +00:00",
			"updatedAt":"2018-09-19 14:57:46.765 +00:00"
		}
	]*/
}

export function getMonsterById (monsterId) { // OK
	return axios.get(`${URL}/monsters/${monsterId}`).then(({data:monsterData}) => monsterData);	
}

export function getPlayerById (playerId) { // OK
	return axios.get(`${URL}/players/${playerId}`).then(({data:playerData}) => playerData);	
}

export function nextTurn (gameId, cardId) { // OK
	const cardObj = {"card": cardId};

	return axios.post(`${URL}/games/${gameId}/next-turn`, cardObj).then(({data: nextTurnData}) => nextTurnData);         
	/*{
		"game": {
			"id": "8e975b7a-2355-4f51-830b-68e4211b80f3",
			"currentTurn": 1,
			"maxTurns": 20,
			"turnsLeft": 19,
			"createdAt": "2018-09-19T14:57:46.747Z",
			"updatedAt": "2018-09-19T14:57:46.747Z"
		},
		"monsterEffect": {
			"effect": "DAMAGE",
			"value": 3
		}
	}*/
}