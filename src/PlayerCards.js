import React from 'react';
import PropTypes from 'prop-types';
import cardDefaultAvatar from './img/card__avatar_default.png';
import './PlayerCards.css';

const lowerCaseAndCapitalize = (string) => string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();

function Card (props) {
	const { value, effect, handleSelect, dataId, chosen, horror} = props;

	return (
		<button type='button' onClick={handleSelect}  disabled={horror ? true : false} data-id={dataId} className={`player-card p-4 shadow btn btn-light ${chosen ? 'active' : ''}`}>
			<img src={cardDefaultAvatar} className='img-fluid' alt={`${effect} ${value} HP card`}/>
			<p className='h4 mt-3'>{lowerCaseAndCapitalize(effect)}</p>
			<span>{value} HP</span>
		</button>
	);
}

Card.propTypes = {
	effect: PropTypes.string.isRequired,
	value: PropTypes.number.isRequired,
	handleSelect: PropTypes.func.isRequired,
	dataId: PropTypes.string.isRequired,
	chosen: PropTypes.bool.isRequired,
	horror: PropTypes.number.isRequired
}

function PlayerCards(props) {
	const { cards, handleCards, chosenCardId, horror } = props;
	
	const ArrayOfCards = cards.map(({id, value, effect}) => {
		return <div className='col-4 mb-4' key={id}>
			<Card handleSelect={handleCards} horror={horror} chosen={chosenCardId === id ? true : false } dataId={id} value={value} effect={effect} />
		</div>
	});
	
	return (
		<div className='row'>
			{ ArrayOfCards }
		</div>
	);
}

PlayerCards.propTypes = {
	cards: PropTypes.array.isRequired,
	handleCards: PropTypes.func.isRequired,
	chosenCardId: PropTypes.string,
	horror: PropTypes.number.isRequired
}

export default PlayerCards