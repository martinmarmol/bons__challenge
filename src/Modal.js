import React from 'react';

function Modal (props) {
	const { children } = props;
	return (
		<div className='gameboard__modal rounded bg-light container p-5'>
			<div className='row'>
				<div className='col'>
					{children}
				</div>
			</div>
		</div>
	);
}

export default Modal